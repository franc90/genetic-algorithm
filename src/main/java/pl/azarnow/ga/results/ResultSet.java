package pl.azarnow.ga.results;

import com.panayotis.gnuplot.dataset.DataSet;
import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.population.Population;
import pl.azarnow.ga.utils.CommonSettings;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ResultSet implements DataSet {
    private final int iterationNr;
    private List<Result> results;
    private List<Point> totalPoints;

    public ResultSet(int iterationNr) {
        this.iterationNr = iterationNr;
        this.results = new ArrayList<>();
        this.totalPoints = new ArrayList<>();
    }

    public int getIterationNr() {
        return iterationNr;
    }

    public List<Result> getResults() {
        return results;
    }

    public void addResult(Population<Chromosome> population) {
        Result result = getResult(population.getPopulationNr());

        for (Chromosome chromosome : population.getChromosomes()) {
            Point point = PointConverter.convertToPoint(chromosome);
            totalPoints.add(point);
            result.addPoint(point);
        }

        results.add(result);
    }

    private Result getResult(int populationNr) {
        Iterator<Result> iter = results.iterator();
        while (iter.hasNext()) {
            Result next = iter.next();
            if (next.getPopulationNr() == populationNr) {
                iter.remove();
                next.removeAll();
                return next;
            }
        }

        return new Result(populationNr);
    }

    @Override
    public int size() {
        return totalPoints.size();
    }

    @Override
    public int getDimensions() {
        return CommonSettings.getGenotypeSize() + 1;
    }

    @Override
    public String getPointValue(int pointNr, int dimension) {
        Point point = totalPoints.get(pointNr);
        List<Double> coords = point.getCoords();
        if (dimension >= coords.size()) {
            return String.valueOf(point.getFitnessVal());
        }
        return String.valueOf(coords.get(dimension));
    }

    @Override
    public String toString() {
        return "ResultSet{" +
                "iterationNr=" + iterationNr +
                ", results=" + results +
                '}';
    }
}
