package pl.azarnow.ga.results;

import java.util.List;

public class Point {

    private final List<Double> coords;
    private final Double fitnessVal;

    public Point(List<Double> coords, Double fitnessVal) {
        this.coords = coords;
        this.fitnessVal = fitnessVal;
    }

    public List<Double> getCoords() {
        return coords;
    }

    public Double getFitnessVal() {
        return fitnessVal;
    }


    public static class PointBuilder {
        private List<Double> coords;
        private double fitness;

        public PointBuilder coords(List<Double> coords) {
            this.coords = coords;
            return this;
        }

        public PointBuilder fitness(double fitness) {
            this.fitness = fitness;
            return this;
        }


        public Point build() {
            return new Point(coords, fitness);
        }

    }

    @Override
    public String toString() {
        return String.valueOf(fitnessVal);
    }
}
