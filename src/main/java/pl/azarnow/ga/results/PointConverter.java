package pl.azarnow.ga.results;

import pl.azarnow.ga.chromosomes.Chromosome;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PointConverter {

    public static Point convertToPoint(Chromosome chromosome) {
        List<Double> coords = new ArrayList<>(chromosome.getGenotype().size());

        coords.addAll(chromosome.getGenotype().stream().collect(Collectors.toList()));

        return new Point.PointBuilder()
                .coords(coords)
                .fitness(chromosome.getFitness())
                .build();
    }
}