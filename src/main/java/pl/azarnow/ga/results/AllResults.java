package pl.azarnow.ga.results;

import pl.azarnow.ga.population.Population;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AllResults {

    private List<ResultSet> results;

    public AllResults() {
        this.results = new ArrayList<>();
    }

    public List<ResultSet> getResults() {
        return results;
    }

    public ResultSet getResult(int id) {
        return results.get(id);
    }

    public ResultSet getResultByIteration(int iterationNr) {
        for (ResultSet result : results) {
            if (result.getIterationNr() == iterationNr)
                return result;
        }
        return null;
    }

    public void addResult(Integer iterationNr, Population population) {
        ResultSet result = getResultByIteration(iterationNr);
        if (result == null) {
            result = new ResultSet(iterationNr);
            results.add(result);
        }

        result.addResult(population);
    }

    public void saveToFile(String filename) throws FileNotFoundException {
        PrintWriter out = new PrintWriter(filename);

        out.print("AllResults = [\n");
        for (ResultSet resultSet : results) {
            out.print("\t");
            out.print(resultSet.getIterationNr());
            out.print(" = [\n");
            for (Result result : resultSet.getResults()) {
                out.print("\t\t");
                out.print(result.getPopulationNr());
                out.print(" = [\n");
                for (Point point : result.getPoints()) {
                    out.print("\t\t\t");
                    for (Double coord : point.getCoords()) {
                        out.print(coord);
                        out.print('\t');
                    }
                    out.print('\n');
                }
                out.print("\t\t];\n");
            }
            out.print("\t];\n");
        }
        out.print("];");

        out.close();
    }

    public void saveAsCsvToFile(String filename, Integer... iterations) throws FileNotFoundException {
        List<Integer> iters = new ArrayList<>(Arrays.asList(iterations));

        for (ResultSet resultSet : results) {
            if (iters.contains(resultSet.getIterationNr())) {
                for (Result result : resultSet.getResults()) {
                    int populationNr = result.getPopulationNr();
                    PrintWriter out = new PrintWriter(filename + "_" + resultSet.getIterationNr() + "_" + populationNr + ".txt");

                    for (Point point : result.getPoints()) {
                        for (Double coord : point.getCoords()) {
                            out.print(coord + "\t");
                        }
                        out.println(point.getFitnessVal());
                    }

                    out.close();
                }
            }
        }
    }

    @Override
    public String toString() {
        return "AllResults{" +
                "results=" + results +
                '}';
    }
}
