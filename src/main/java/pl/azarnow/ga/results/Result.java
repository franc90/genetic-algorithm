package pl.azarnow.ga.results;

import com.panayotis.gnuplot.dataset.DataSet;
import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.population.Population;
import pl.azarnow.ga.utils.CommonSettings;

import java.util.ArrayList;
import java.util.List;

public class Result implements DataSet {

    private List<Point> points;

    private final int populationNr;

    public Result(int populationNr) {
        this.populationNr = populationNr;
        points = new ArrayList<>(CommonSettings.getPopulationSize());
    }

    public List<Point> getPoints() {
        return points;
    }

    public int getPopulationNr() {
        return populationNr;
    }

    public void removeAll() {
        points.clear();
    }

    public void addPoints(Population p) {
        List<Chromosome> chromosomes = p.getChromosomes();
        for (Chromosome chromosome : chromosomes) {
            points.add(PointConverter.convertToPoint(chromosome));
        }
    }

    public void addPoints(List<Point> points) {
        points.addAll(points);
    }

    public void addPoint(Chromosome chromosome) {
        points.add(PointConverter.convertToPoint(chromosome));
    }

    public void addPoint(Point point) {
        points.add(point);
    }

    @Override
    public String toString() {
        return "Result{" +
                "points=" + points +
                '}';
    }

    @Override
    public int size() {
        return points.size();
    }

    @Override
    public int getDimensions() {
        return CommonSettings.getGenotypeSize() + 1;
    }

    @Override
    public String getPointValue(int point, int dimension) {
        Point p = points.get(point);
        List<Double> coords = p.getCoords();

        if (coords.size() > dimension) {
            return String.valueOf(coords.get(dimension));
        }

        return String.valueOf(p.getFitnessVal());
    }
}
