package pl.azarnow.ga.plot;

import com.panayotis.gnuplot.JavaPlot;
import com.panayotis.gnuplot.plot.DataSetPlot;
import com.panayotis.gnuplot.plot.FunctionPlot;
import com.panayotis.gnuplot.style.PlotStyle;
import com.panayotis.gnuplot.style.Style;
import com.panayotis.gnuplot.terminal.ImageTerminal;
import pl.azarnow.ga.results.Result;
import pl.azarnow.ga.results.ResultSet;
import pl.azarnow.ga.utils.CommonSettings;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class Plot {

    private int windowWidth = 1024;
    private int windowHeight = 768;
    private boolean separatePopulations = false;

    private JavaPlot p;

    public Plot() {
    }

    public Plot(boolean separatePopulations) {
        this.separatePopulations = separatePopulations;
    }

    public void setWindowWidth(int windowWidth) {
        this.windowWidth = windowWidth;
    }

    public void setWindowHeight(int windowHeight) {
        this.windowHeight = windowHeight;
    }

    public void plot(ResultSet... resultSets) {
        initPlot();

        for (int i = 0; i < resultSets.length; ++i) {
            addResultSet(i, resultSets[i]);
        }

        p.setTitle(getTitle(resultSets));
        p.plot();
    }

    public void plotToFile(ResultSet resultSet, String pathToFile, String fileName) {
        initPlot();

        addResultSet(0, resultSet);

        ImageTerminal png = new ImageTerminal();
        File file = new File(pathToFile + File.separator + fileName + ".png");
        try {
            file.createNewFile();
            png.processOutput(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            p.setTerminal(png);
            p.setTitle(getTitle(resultSet));
            p.plot();
            ImageIO.write(png.getImage(), "png", file);
        } catch (IOException ex) {
            System.err.println("Could no write to file: " + pathToFile + File.separator + fileName);
        }
    }

    private void initPlot() {
        p = new JavaPlot(true);

        p.set("terminal wxt size", buildWindowSize());
        p.set("view", "map");
        p.set("isosamples", "150,150");
        p.set("cntrparam levels auto", String.valueOf(10));
        p.set("xrange", buildRange());
        p.set("yrange", buildRange());
        p.getParameters().getPostInit().add("unset clabel");
        p.getParameters().getPostInit().add("unset key");

        FunctionPlot plot = new FunctionPlot(getFitness());
        plot.setPlotStyle(new PlotStyle(Style.PM3D));

        p.addPlot(plot);
    }

    private void addResultSet(int resultSetNr, ResultSet resultSet) {
        if (separatePopulations) {
            List<Result> results = resultSet.getResults();
            for (int i = 0; i < results.size(); i++) {
                DataSetPlot customDataSet = new DataSetPlot(results.get(i));
                customDataSet.setPlotStyle(new CustomPointStyle(i));
                p.addPlot(customDataSet);
            }
        } else {
            DataSetPlot customDataSet = new DataSetPlot(resultSet);
            customDataSet.setPlotStyle(new CustomPointStyle(resultSetNr));
            p.addPlot(customDataSet);
        }
    }

    private String getTitle(ResultSet... resultSets) {
        StringBuilder builder = new StringBuilder("Results for iteration");

        if (resultSets.length > 1) {
            builder.append("s");
        }
        builder.append(": ");

        for (ResultSet resultSet : resultSets) {
            builder
                    .append(resultSet.getIterationNr())
                    .append(", ");
        }
        return builder.substring(0, builder.length() - 2);
    }

    private String buildRange() {
        return "[" + CommonSettings.getLowerBound() + ":" + CommonSettings.getUpperBound() + "]";
    }

    private String buildWindowSize() {
        return windowWidth + "," + windowHeight;
    }

    public String getFitness() {
        return CommonSettings.getFitnessFunction().getStringFormula();
    }
}
