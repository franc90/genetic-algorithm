package pl.azarnow.ga.plot;

import com.panayotis.gnuplot.style.PlotColor;
import com.panayotis.gnuplot.style.PlotStyle;
import com.panayotis.gnuplot.style.RgbPlotColor;
import com.panayotis.gnuplot.style.Style;

public class CustomPointStyle extends PlotStyle {

    private static RgbPlotColor[] colors = {new RgbPlotColor(0, 255, 0), new RgbPlotColor(255, 255, 255), new RgbPlotColor(255, 255, 0), new RgbPlotColor(127, 255, 0), new RgbPlotColor(107, 142, 35)};

    public CustomPointStyle() {
        this(0);
    }

    public CustomPointStyle(int index) {
        this(colors[index % colors.length]);
    }

    public CustomPointStyle(PlotColor plotColor) {
        super(Style.POINTS);
        setPointType(7);
        setPointSize(1);
        setLineType(plotColor);
    }
}
