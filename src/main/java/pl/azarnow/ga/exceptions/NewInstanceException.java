package pl.azarnow.ga.exceptions;

public class NewInstanceException extends RuntimeException {

    public NewInstanceException(String message, Throwable cause) {
        super(message, cause);
    }

    public NewInstanceException() {
    }

    public NewInstanceException(String message) {
        super(message);
    }

    public NewInstanceException(Throwable cause) {
        super(cause);
    }
}
