package pl.azarnow.ga.experiments;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import pl.azarnow.ga.algorithm.Algorithm;
import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.functions.crossover.ArithmeticCrossover;
import pl.azarnow.ga.functions.migration.SteppingStoneMigration;
import pl.azarnow.ga.functions.mutation.DynamicMutation;
import pl.azarnow.ga.functions.selection.TournamentSelection;
import pl.azarnow.ga.plot.Plot;
import pl.azarnow.ga.population.Population;
import pl.azarnow.ga.results.AllResults;
import pl.azarnow.ga.utils.CommonSettings;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Experiment {

    private int[] plotIterationNumbers = {1, 2, 5, 10, 20, 50, 75, 100, 200};

    private int repetitions;

    private String outputFile;

    private List<Double> mutationProbabilities;

    private List<Integer> iterationsCount;

    private List<Integer> islandIterations;

    private List<Integer> totalPopulations;

    private List<Integer> businessmenPopulation;

    private List<Integer> clientPopulations;

    protected Algorithm algorithm;

    protected Population population;

    protected String name;

    protected String directory;

    public void experiment() {
        int totalCount = getTotalCount();
        int cnt = 0;

        for (Double mutationProbability : mutationProbabilities) {
            CommonSettings.setMutationProbability(mutationProbability);
            for (Integer iterations : iterationsCount) {
                CommonSettings.setTotalIterations(iterations);
                for (Integer islandIteration : islandIterations) {
                    CommonSettings.setIslandsIterations(islandIteration);

                    if (totalPopulations != null) {
                        for (Integer totalPopulationSize : totalPopulations) {
                            CommonSettings.setPopulationSize(totalPopulationSize);
                            startExperiment(mutationProbability, iterations, islandIteration, totalPopulationSize);
                            plot(generateFilename(mutationProbability, iterations, islandIteration, totalPopulationSize));
                            System.out.println(name + ": " + ++cnt + " out of " + totalCount);
                        }
                    } else {
                        for (Integer businessmen : businessmenPopulation) {
                            for (Integer clients : clientPopulations) {
                                if (clients >= businessmen) {
                                    CommonSettings.setBusinessmenAndClientsPopulationSize(businessmen, clients);
                                    startExperiment(mutationProbability, iterations, islandIteration, businessmen, clients);
                                    plot(generateFilename(mutationProbability, iterations, islandIteration, businessmen, clients));
                                    System.out.println(name + ": " + ++cnt + " out of " + totalCount);
                                } else {
                                    System.out.println(name + ": Skipping " + ++cnt + " out of " + totalCount);
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    private void plot(String filename) {
        AllResults allResults = CommonSettings.getAllResults();
        int resultsSize = allResults.getResults().size();

        Plot plot = new Plot(true);
        for (int i = 0; i < plotIterationNumbers.length && plotIterationNumbers[i] < resultsSize; ++i) {
            plot.plotToFile(allResults.getResult(plotIterationNumbers[i]), directory, filename + i);
        }
    }

    private String generateFilename(Double mutationProbability, Integer iterations, Integer islandIteration, Integer... populations) {
        StringBuilder builder = new StringBuilder("mutation")
                .append(mutationProbability)
                .append("_iters")
                .append(iterations)
                .append("_islIters")
                .append(islandIteration);

        for (int i = 0; i < populations.length; ++i) {
            builder.append("_population")
                    .append(i + 1)
                    .append("-")
                    .append(populations[i]);
        }

        builder.append("__");
        return builder.toString();
    }

    private int getTotalCount() {
        int total = mutationProbabilities.size();
        total *= iterationsCount.size();
        total *= islandIterations.size();

        if (totalPopulations != null) {
            total *= totalPopulations.size();
        } else {
            total *= businessmenPopulation.size();
            total *= clientPopulations.size();
        }

        return total;
    }

    private void startExperiment(Double mutationProbability, Integer iterations, Integer islandIteration, Integer... populations) {
        CommonSettings.setAllResults(new AllResults());

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < repetitions; ++i) {
            setAlgorithmAndPopulation();

            algorithm.setCrossover(new ArithmeticCrossover());
            algorithm.setMutation(new DynamicMutation());
            algorithm.setSelection(new TournamentSelection());
            algorithm.setMigration(new SteppingStoneMigration());

            long start = System.currentTimeMillis();
            Population pop = algorithm.evolve(population);
            long end = System.currentTimeMillis();

            Chromosome fittest = pop.getFittest();

            builder.append(i)
                    .append(',')
                    .append(mutationProbability)
                    .append(',')
                    .append(iterations)
                    .append(',')
                    .append(islandIteration)
                    .append(',')
                    .append(StringUtils.join(populations, ','))
                    .append(',')
                    .append(fittest.getGenotype().get(0))
                    .append(',')
                    .append(fittest.getGenotype().get(1))
                    .append(',')
                    .append(fittest.getFitness())
                    .append(',')
                    .append(end - start)
                    .append('\n');
        }

        try {
            FileUtils.writeStringToFile(new File(outputFile), builder.append('\n').toString(), true);
        } catch (IOException e) {
            System.err.println("Could not write to file: " + builder.toString());
        }
    }

    protected abstract void setAlgorithmAndPopulation();

    public Experiment withMutationProbabilities(Double... probabilities) {
        mutationProbabilities = new ArrayList<>(probabilities.length);
        mutationProbabilities.addAll(Arrays.asList(probabilities));
        return this;
    }

    public Experiment withIterations(Integer... iterations) {
        this.iterationsCount = new ArrayList<>(iterations.length);
        this.iterationsCount.addAll(Arrays.asList(iterations));
        return this;
    }

    public Experiment withIslandIterations(Integer... islandIterations) {
        this.islandIterations = new ArrayList<>(islandIterations.length);
        this.islandIterations.addAll(Arrays.asList(islandIterations));
        return this;
    }

    public Experiment withTotalPopulations(Integer... totalPopulations) {
        this.totalPopulations = new ArrayList<>(totalPopulations.length);
        this.totalPopulations.addAll(Arrays.asList(totalPopulations));
        return this;
    }

    public Experiment withBusinessmenPopulations(Integer... businessmenPopulations) {
        this.businessmenPopulation = new ArrayList<>(businessmenPopulations.length);
        this.businessmenPopulation.addAll(Arrays.asList(businessmenPopulations));
        return this;
    }

    public Experiment withClientPopulations(Integer... clientPopulations) {
        this.clientPopulations = new ArrayList<>(clientPopulations.length);
        this.clientPopulations.addAll(Arrays.asList(clientPopulations));
        return this;
    }

    public Experiment withRepetitions(int repetitions) {
        this.repetitions = repetitions;
        return this;
    }

    public Experiment withOutputFile(String filename) {
        this.outputFile = filename;
        return this;
    }


}
