package pl.azarnow.ga.experiments;

import pl.azarnow.ga.algorithm.CoevolutionarySharedNichingGeneticAlgorithm;
import pl.azarnow.ga.population.CoevolutionarySharedNichingPopulation;

public class CoevolutionarySharedNichingExperiment extends Experiment{
    @Override
    protected void setAlgorithmAndPopulation() {
        algorithm = new CoevolutionarySharedNichingGeneticAlgorithm();
        population = new CoevolutionarySharedNichingPopulation(true);
        name = "Coev";
        directory = "out/coev";
    }
}
