package pl.azarnow.ga.experiments;

import pl.azarnow.ga.algorithm.ProbabilisticCrowdingIslandModel;
import pl.azarnow.ga.population.Population;

public class ProbabilisticCrowdingExperiment extends Experiment {
    @Override
    protected void setAlgorithmAndPopulation() {
        algorithm = new ProbabilisticCrowdingIslandModel();
        population = new Population(true);
        name = "Probabilistic";
        directory = "out/probabilisticCrowding";
    }
}
