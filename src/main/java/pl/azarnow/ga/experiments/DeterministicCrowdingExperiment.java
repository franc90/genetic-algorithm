package pl.azarnow.ga.experiments;

import pl.azarnow.ga.algorithm.DeterministicCrowdingIslandModel;
import pl.azarnow.ga.population.Population;

public class DeterministicCrowdingExperiment extends Experiment {
    @Override
    protected void setAlgorithmAndPopulation() {
        algorithm = new DeterministicCrowdingIslandModel();
        population = new Population(true);
        name = "Deterministic";
        directory = "out/deterministicCrowding";
    }
}
