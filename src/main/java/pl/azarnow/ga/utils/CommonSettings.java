package pl.azarnow.ga.utils;

import pl.azarnow.ga.chromosomes.factories.strategies.ChromosomeFactoryStrategy;
import pl.azarnow.ga.functions.fitness.FitnessFunction;
import pl.azarnow.ga.results.AllResults;

public class CommonSettings {

    private static int totalIterations;

    private static int genotypeSize;

    private static int populationSize;

    private static int businessmenPopulationSize;

    private static int clientsPopulationSize;

    private static int tournamentSize;

    private static int islandsNumber;

    private static int islandsIterations;

    private static double rastriginConstant;

    private static double lowerBound;

    private static double upperBound;

    private static double mutationProbability;

    private static double geneMutationProbability;

    private static boolean selectedOnceInIteration;

    private static FitnessFunction fitnessFunction;

    private static ChromosomeFactoryStrategy chromosomeFactoryStrategy;

    private static AllResults allResults;

    public static int getTotalIterations() {
        return totalIterations;
    }

    public static void setTotalIterations(int totalIterations) {
        CommonSettings.totalIterations = totalIterations;
    }

    public static int getGenotypeSize() {
        return genotypeSize;
    }

    public static void setGenotypeSize(int genotypeSize) {
        CommonSettings.genotypeSize = genotypeSize;
    }

    public static int getPopulationSize() {
        return populationSize;
    }

    public static void setPopulationSize(int populationSize) {
        CommonSettings.populationSize = populationSize;
    }

    public static int getBusinessmenPopulationSize() {
        return businessmenPopulationSize;
    }

    public static void setBusinessmenPopulationSize(int businessmenPopulationSize) {
        CommonSettings.businessmenPopulationSize = businessmenPopulationSize;
        CommonSettings.populationSize = businessmenPopulationSize;
    }

    public static int getClientsPopulationSize() {
        return clientsPopulationSize;
    }

    public static void setClientsPopulationSize(int clientsPopulationSize) {
        CommonSettings.clientsPopulationSize = clientsPopulationSize;
        CommonSettings.populationSize += clientsPopulationSize;
    }

    public static void setBusinessmenAndClientsPopulationSize(int businessmenPopulationSize, int clientsPopulationSize) {
        CommonSettings.populationSize = businessmenPopulationSize + clientsPopulationSize;
        CommonSettings.businessmenPopulationSize = businessmenPopulationSize;
        CommonSettings.clientsPopulationSize = clientsPopulationSize;
    }

    public static int getTournamentSize() {
        return tournamentSize;
    }

    public static void setTournamentSize(int tournamentSize) {
        CommonSettings.tournamentSize = tournamentSize;
    }

    public static int getIslandsNumber() {
        return islandsNumber;
    }

    public static void setIslandsNumber(int islandsNumber) {
        CommonSettings.islandsNumber = islandsNumber;
    }

    public static int getIslandsIterations() {
        return islandsIterations;
    }

    public static void setIslandsIterations(int islandsIterations) {
        CommonSettings.islandsIterations = islandsIterations;
    }

    public static double getRastriginConstant() {
        return rastriginConstant;
    }

    public static void setRastriginConstant(double rastriginConstant) {
        CommonSettings.rastriginConstant = rastriginConstant;
    }

    public static double getLowerBound() {
        return lowerBound;
    }

    public static void setLowerBound(double lowerBound) {
        CommonSettings.lowerBound = lowerBound;
    }

    public static double getUpperBound() {
        return upperBound;
    }

    public static void setUpperBound(double upperBound) {
        CommonSettings.upperBound = upperBound;
    }

    public static double getMutationProbability() {
        return mutationProbability;
    }

    public static void setMutationProbability(double mutationProbability) {
        CommonSettings.mutationProbability = mutationProbability;
    }

    public static double getGeneMutationProbability() {
        return geneMutationProbability;
    }

    public static void setGeneMutationProbability(double geneMutationProbability) {
        CommonSettings.geneMutationProbability = geneMutationProbability;
    }

    public static boolean isSelectedOnceInIteration() {
        return selectedOnceInIteration;
    }

    public static void setSelectedOnceInIteration(boolean selectedOnceInIteration) {
        CommonSettings.selectedOnceInIteration = selectedOnceInIteration;
    }

    public static FitnessFunction getFitnessFunction() {
        return fitnessFunction;
    }

    public static void setFitnessFunction(FitnessFunction fitnessFunction) {
        CommonSettings.fitnessFunction = fitnessFunction;
    }

    public static ChromosomeFactoryStrategy getChromosomeFactoryStrategy() {
        return chromosomeFactoryStrategy;
    }

    public static void setChromosomeFactoryStrategy(ChromosomeFactoryStrategy chromosomeFactoryStrategy) {
        CommonSettings.chromosomeFactoryStrategy = chromosomeFactoryStrategy;
    }

    public static AllResults getAllResults() {
        return allResults;
    }

    public static void setAllResults(AllResults allResults) {
        CommonSettings.allResults = allResults;
    }
}
