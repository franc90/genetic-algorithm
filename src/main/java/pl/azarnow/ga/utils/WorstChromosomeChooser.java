package pl.azarnow.ga.utils;

import pl.azarnow.ga.chromosomes.Chromosome;

import java.util.ArrayList;
import java.util.Collections;

public class WorstChromosomeChooser extends ArrayList<Chromosome> {

    public Chromosome findWorstChromosome() {
        Collections.sort(this);
        return get(size() - 1);
    }

}
