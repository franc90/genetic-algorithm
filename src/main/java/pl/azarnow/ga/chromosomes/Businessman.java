package pl.azarnow.ga.chromosomes;

import java.util.ArrayList;
import java.util.List;

public class Businessman extends CoevolutionarySharedNichingChromosome {

    private double fitnessValue;

    private List<Client> clients = new ArrayList<>();

    public Businessman(List<Double> genotype) {
        super(genotype, ChromosomeType.Businessman);
    }

    public int getClientsCount() {
        return clients.size();
    }

    @Override
    public void computeFitness() {
        fitnessValue = .0;

        for (Client client : clients) {
            fitnessValue += client.getFitness();
        }
    }

    @Override
    public double getOriginalFitness() {
        return super.getFitness();
    }


    public List<Client> getClients() {
        return clients;
    }

    @Override
    public double getFitness() {
        return fitnessValue;
    }
}
