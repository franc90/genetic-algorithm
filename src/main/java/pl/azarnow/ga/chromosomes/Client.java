package pl.azarnow.ga.chromosomes;

import pl.azarnow.ga.population.Population;

import java.util.List;

public class Client extends CoevolutionarySharedNichingChromosome {

    private Businessman businessman;

    private double fitnessVal;

    public Client(List<Double> genotype) {
        super(genotype, ChromosomeType.Client);
    }

    public void findClosestBuisnessman(Population population) {
        double bestDistance = Double.MAX_VALUE;

        for (Chromosome chromosome : (List<Chromosome>)population.getChromosomes()) {
            double distance = distance(chromosome);
            if (distance < bestDistance) {
                bestDistance = distance;
                businessman = (Businessman) chromosome;
            }
        }

        businessman.getClients().add(this);
    }

    @Override
    public void computeFitness() {
        double fitness = super.getFitness();
        fitnessVal = fitness * businessman.getClientsCount();
    }

    @Override
    public double getOriginalFitness() {
        return super.getFitness();
    }

    @Override
    public double getFitness() {
        return fitnessVal;
    }

    @Override
    public String toString() {
        String val = super.toString();
        return getParents().isEmpty() + "   " + val + "; " + super.getFitness();
    }
}
