package pl.azarnow.ga.chromosomes;

import java.util.List;

public abstract class CoevolutionarySharedNichingChromosome extends Chromosome {

    public CoevolutionarySharedNichingChromosome(List<Double> genotype, ChromosomeType chromosomeType) {
        super(genotype, chromosomeType);
    }

    public abstract void computeFitness();

    public abstract double getOriginalFitness();

}
