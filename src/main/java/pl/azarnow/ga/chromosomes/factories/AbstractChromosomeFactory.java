package pl.azarnow.ga.chromosomes.factories;

import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.exceptions.NewInstanceException;
import pl.azarnow.ga.utils.CommonSettings;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class AbstractChromosomeFactory<T extends Chromosome> {

    private Class<T> clazz;
    private Random random = new Random();

    public AbstractChromosomeFactory(Class<T> clazz) {
        this.clazz = clazz;
    }

    public T newInstance(List<Double> genotype) {
        try {
            return clazz.getDeclaredConstructor(List.class).newInstance(genotype);
        } catch (Exception e) {
            throw new NewInstanceException(e);
        }

    }

    public T newInstance() {
        List<Double> genotype = generateGenotype();

        return newInstance(genotype);
    }

    protected List<Double> generateGenotype() {
        List<Double> genotype = new ArrayList<>(CommonSettings.getGenotypeSize());

        double UB = CommonSettings.getUpperBound();
        double LB = CommonSettings.getLowerBound();

        for (int i = 0; i < CommonSettings.getGenotypeSize(); ++i) {
            double gene = random.nextDouble() * (UB - LB) + LB;
            genotype.add(gene);
        }

        return genotype;
    }

}
