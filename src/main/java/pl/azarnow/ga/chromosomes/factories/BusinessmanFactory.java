package pl.azarnow.ga.chromosomes.factories;

import pl.azarnow.ga.chromosomes.Businessman;

public class BusinessmanFactory extends AbstractChromosomeFactory<Businessman> {

    public BusinessmanFactory() {
        super(Businessman.class);
    }

}
