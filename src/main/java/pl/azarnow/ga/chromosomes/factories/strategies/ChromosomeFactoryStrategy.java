package pl.azarnow.ga.chromosomes.factories.strategies;

import pl.azarnow.ga.chromosomes.ChromosomeType;
import pl.azarnow.ga.chromosomes.factories.AbstractChromosomeFactory;
import pl.azarnow.ga.exceptions.NullValueException;

import java.util.HashMap;
import java.util.Map;

public class ChromosomeFactoryStrategy {

    private final Map<ChromosomeType, AbstractChromosomeFactory> factories;

    public ChromosomeFactoryStrategy(Map<ChromosomeType, AbstractChromosomeFactory> factories) {
        this.factories = factories;
    }

    public AbstractChromosomeFactory getDefaultFactory() {
        return getFactory(ChromosomeType.Default);
    }

    public AbstractChromosomeFactory getFactory(ChromosomeType chromosomeType) {
        AbstractChromosomeFactory factory = factories.get(chromosomeType);

        if (factory == null) {
            throw new NullValueException("No factory found for " + chromosomeType);
        }

        return factory;
    }

    public static class ChromosomeFactoryStrategyBuilder {
        private Map<ChromosomeType, AbstractChromosomeFactory> factories = new HashMap<>();

        public ChromosomeFactoryStrategyBuilder withFactoryFor(AbstractChromosomeFactory factory, ChromosomeType... chromosomeTypes) {
            for (ChromosomeType chromosomeType : chromosomeTypes) {
                factories.put(chromosomeType, factory);
            }
            return this;
        }

        public ChromosomeFactoryStrategy build() {
            return new ChromosomeFactoryStrategy(factories);
        }
    }

}
