package pl.azarnow.ga.chromosomes.factories;

import pl.azarnow.ga.chromosomes.Chromosome;

public class SimpleChromosomeFactory extends AbstractChromosomeFactory<Chromosome> {

    public SimpleChromosomeFactory() {
        super(Chromosome.class);
    }

}
