package pl.azarnow.ga.chromosomes.factories;

import pl.azarnow.ga.chromosomes.Client;

public class ClientFactory extends AbstractChromosomeFactory<Client> {

    public ClientFactory() {
        super(Client.class);
    }
}
