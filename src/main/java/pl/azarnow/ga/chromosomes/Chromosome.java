package pl.azarnow.ga.chromosomes;

import pl.azarnow.ga.utils.CommonSettings;

import java.util.ArrayList;
import java.util.List;

public class Chromosome implements Comparable<Chromosome> {

    private final List<Double> genotype;

    private final List<Chromosome> parents;

    private final ChromosomeType chromosomeType;

    public Chromosome(List<Double> genotype) {
        this(genotype, ChromosomeType.Simple);
    }

    public Chromosome(List<Double> genotype, ChromosomeType chromosomeType) {
        this.genotype = genotype;
        this.parents = new ArrayList<>();
        this.chromosomeType = chromosomeType;
    }

    public double getFitness() {
        return CommonSettings.getFitnessFunction().computeFitness(this);
    }

    public List<Double> getGenotype() {
        return genotype;
    }

    public List<Chromosome> getParents() {
        return parents;
    }

    public ChromosomeType getChromosomeType() {
        return chromosomeType;
    }

    public double distance(Chromosome c) {
        if (genotype.size() > c.genotype.size()) {
            return Double.MIN_VALUE;
        }

        double sum = .0;

        for (int i = 0; i < genotype.size(); i++) {
            double v1 = genotype.get(i);
            double v2 = c.genotype.get(i);
            sum += (v2 - v1) * (v2 - v1);
        }

        return sum;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("[");

        for (Double val : genotype) {
            builder.append(val);
            builder.append(", ");
        }

        return builder.delete(builder.length()-2, builder.length()-1)
                .append("] = ")
                .append(getFitness())
                .toString();
    }

    @Override
    public int compareTo(Chromosome o) {
        return Double.compare(getFitness(), o.getFitness());
    }
}
