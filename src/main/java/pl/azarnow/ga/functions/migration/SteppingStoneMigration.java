package pl.azarnow.ga.functions.migration;

import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.population.Population;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class SteppingStoneMigration implements Migration {

    private Random random = new Random();

    @Override
    public List<Population> migrate(List<Population> populations) {
        List<Integer> populationsNr = new ArrayList<>(populations.size());

        for (int i = 0; i < populations.size(); i++) {
            populationsNr.add(i);
        }
        Collections.shuffle(populationsNr);

        int cnt = 0;
        for (Population<Chromosome> population : populations) {
            Chromosome chosen = population.getChromosomes().remove(random.nextInt(population.size()));
            populations.get(populationsNr.get(cnt++)).getChromosomes().add(chosen);
            cnt = cnt % populationsNr.size();
        }

        return populations;
    }

}
