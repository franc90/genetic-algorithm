package pl.azarnow.ga.functions.migration;

import pl.azarnow.ga.population.Population;

import java.util.List;

public interface Migration {

    List<Population> migrate(List<Population> populations);

}
