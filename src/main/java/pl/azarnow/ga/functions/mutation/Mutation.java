package pl.azarnow.ga.functions.mutation;

import pl.azarnow.ga.chromosomes.Chromosome;

public interface Mutation {

    Chromosome mutate(Chromosome chromosome, int iterationNr);

}
