package pl.azarnow.ga.functions.mutation;

import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.utils.CommonSettings;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static java.lang.Math.pow;

public class DynamicMutation implements Mutation {

    private Random random = new Random();

    @Override
    public Chromosome mutate(Chromosome chromosome, int iterationNr) {
        List<Double> genotype = new ArrayList<>(chromosome.getGenotype().size());

        for (int i = 0; i < chromosome.getGenotype().size(); i++) {
            Double gene = chromosome.getGenotype().get(i);

            if (random.nextDouble() < CommonSettings.getGeneMutationProbability()) {
                double delta = delta(iterationNr, gene);
                gene += delta;
            }

            genotype.add(gene);
        }

        return CommonSettings.getChromosomeFactoryStrategy().getFactory(chromosome.getChromosomeType()).newInstance(genotype);
    }

    private double delta(int iterationNr, double currentGeneValue) {
        boolean upperBound = random.nextBoolean();
        double newVal;
        if (upperBound) {
            newVal = CommonSettings.getUpperBound() - currentGeneValue;
        } else {
            newVal = currentGeneValue - CommonSettings.getLowerBound();
        }

        double ex1 = 1 - ((double) iterationNr) / CommonSettings.getTotalIterations();
        double ex2 = pow(ex1, 5);
        double r = pow(random.nextDouble(), ex2);
        double newDelta = newVal * (1 - r);
        return upperBound ? newDelta : -newDelta;
    }
}
