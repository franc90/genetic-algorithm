package pl.azarnow.ga.functions.fitness;

import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.utils.CommonSettings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.Math.PI;
import static java.lang.Math.cos;

public class RastriginFitnessFunction implements FitnessFunction {

    private static final String SIZE = "%SIZE%";
    private static final String X = "%X%";
    private static final String Y = "%Y%";

    private static final String functionRepresentation = "10*" + SIZE + " + " + X + "*" + X +
            " - 10*cos(2*pi*" + X + ") + " + Y + "*" + Y + " - 10*cos(2*pi*" + Y + ")";

    private final List<Double> translations = new ArrayList<>();

    @Override
    public double computeFitness(Chromosome chromosome) {
        double A = CommonSettings.getRastriginConstant();
        double sum = A * CommonSettings.getGenotypeSize();

        for (int i = 0; i < CommonSettings.getGenotypeSize(); ++i) {
            double gene = chromosome.getGenotype().get(i) - getTranslation(i);
            sum += gene * gene - A * cos(2 * PI * gene);
        }

        return sum;
    }

    private double getTranslation(int i) {
        if (i < translations.size()) {
            return translations.get(i);
        }
        return .0;
    }

    @Override
    public String getStringFormula() {
        return functionRepresentation
                .replaceAll(SIZE, String.valueOf(CommonSettings.getGenotypeSize()))
                .replaceAll(X, "(x - " + getTranslation(0) + ")")
                .replaceAll(Y, "(y - " + getTranslation(1) + ")");
    }

    @Override
    public void setParametersTranslation(Double... translations) {
        this.translations.addAll(Arrays.asList(translations));
    }

}
