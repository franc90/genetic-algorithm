package pl.azarnow.ga.functions.fitness;

import pl.azarnow.ga.chromosomes.Chromosome;

public interface FitnessFunction {

    double computeFitness(Chromosome chromosome);

    String getStringFormula();

    void setParametersTranslation(Double... translations);

}
