package pl.azarnow.ga.functions.fitness;

public class FitnessFunctionBuilder {

    public enum FunctionType {
        Rastrigin
    }

    private FitnessFunction function;
    private Double[] translations;

    public FitnessFunctionBuilder function(FunctionType type) {
        if (FunctionType.Rastrigin.equals(type)) {
            function = new RastriginFitnessFunction();
        }
        return this;
    }

    public FitnessFunctionBuilder setParametersTranslation(Double... translations) {
        this.translations = translations;
        return this;
    }

    public FitnessFunction build() {
        function.setParametersTranslation(translations);
        return function;
    }

}
