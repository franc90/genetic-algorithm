package pl.azarnow.ga.functions.selection.roulette;

import pl.azarnow.ga.chromosomes.Chromosome;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class RouletteWheel {

    private Random random = new Random();
    private List<RouletteNumber> numbers = new LinkedList<>();
    private double totalVal = .0;

    public void addNumber(RouletteNumber number) {
        numbers.add(number);
        totalVal += number.getValue();
    }

    public Chromosome getChromosome() {
        double foundNumber = random.nextDouble() * totalVal;

        int sum = 0;
        for (int i = 0; i < numbers.size(); ++i) {
            RouletteNumber number = numbers.get(i);
            sum += number.getValue();
            if (foundNumber <= sum) {
                return number.getChromosome();
            }
        }

        return null;
    }

    public void setRandom(Random random) {
        this.random = random;
    }
}
