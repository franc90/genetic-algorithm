package pl.azarnow.ga.functions.selection;

import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.population.Population;
import pl.azarnow.ga.utils.CommonSettings;

import java.util.Random;

public class TournamentSelection implements Selection {

    private Random random = new Random();

    @Override
    public Chromosome select(Population population) {
        Population tournament = new Population();

        for (int i = 0; i < CommonSettings.getTournamentSize(); ++i) {
            int randomId = random.nextInt(population.size());
            tournament.saveChromosome(i, population.getChromosome(randomId));
        }

        Chromosome chromosome = tournament.getFittest();

        if(CommonSettings.isSelectedOnceInIteration()) {
            population.getChromosomes().remove(chromosome);
        }

        return chromosome;
    }
}
