package pl.azarnow.ga.functions.selection.roulette;

import pl.azarnow.ga.chromosomes.Chromosome;

public class RouletteNumber {

    private Chromosome chromosome;
    private double val;

    public RouletteNumber(Chromosome chromosome) {
        this.chromosome = chromosome;
        this.val = chromosome.getFitness();
    }

    public double getValue() {
        return val;
    }

    public Chromosome getChromosome() {
        return chromosome;
    }

}
