package pl.azarnow.ga.functions.selection;

import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.functions.selection.roulette.RouletteNumber;
import pl.azarnow.ga.functions.selection.roulette.RouletteWheel;
import pl.azarnow.ga.population.Population;
import pl.azarnow.ga.utils.CommonSettings;

import java.util.List;

public class RouletteWheelSelection implements Selection {

    private RouletteWheel rouletteWheel = new RouletteWheel();

    @Override
    public Chromosome select(Population population) {
        for (Chromosome chromosome : (List<Chromosome>) population.getChromosomes()) {
            rouletteWheel.addNumber(new RouletteNumber(chromosome));
        }

        Chromosome chromosome = rouletteWheel.getChromosome();

        if(CommonSettings.isSelectedOnceInIteration()) {
            population.getChromosomes().remove(chromosome);
        }

        return chromosome;
    }

    public void setRouletteWheel(RouletteWheel rouletteWheel) {
        this.rouletteWheel = rouletteWheel;
    }
}
