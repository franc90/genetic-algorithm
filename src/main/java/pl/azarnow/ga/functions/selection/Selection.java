package pl.azarnow.ga.functions.selection;

import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.population.Population;

public interface Selection {

    Chromosome select(Population population);

}
