package pl.azarnow.ga.functions.crossover;

import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.utils.CommonSettings;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CrossoverImpl implements Crossover {

    private Random random = new Random();

    @Override
    public Chromosome crossover(Chromosome c1, Chromosome c2) {
        List<Double> genotype = c1.getGenotype();

        List<Double> newGenotype = new ArrayList<>(genotype.size());

        for (int i = 0; i < genotype.size(); ++i) {
            double gene1 = genotype.get(i);
            double gene2 = c2.getGenotype().get(i);

            if (gene2 < gene1) {
                double tmp = gene1;
                gene1 = gene2;
                gene2 = tmp;
            }

            newGenotype.add(random.nextDouble() * (gene2 - gene1) + gene1);
        }

        return CommonSettings.getChromosomeFactoryStrategy().getFactory(c1.getChromosomeType()).newInstance(newGenotype);
    }
}
