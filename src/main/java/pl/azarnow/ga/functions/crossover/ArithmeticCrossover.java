package pl.azarnow.ga.functions.crossover;

import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.utils.CommonSettings;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ArithmeticCrossover implements Crossover {

    Random random = new Random();

    @Override
    public Chromosome crossover(Chromosome c1, Chromosome c2) {
        List<Double> genotype1 = c1.getGenotype();
        List<Double> genotype2 = c2.getGenotype();

        List<Double> newGenotype = new ArrayList<>(genotype1.size());

        double weight = random.nextDouble();

        for (int i = 0 ; i < genotype1.size() ; ++i) {
            double newVal = weight * genotype1.get(i) + (1-weight) * genotype2.get(i);
            newGenotype.add(newVal);
        }

        return CommonSettings.getChromosomeFactoryStrategy().getFactory(c1.getChromosomeType()).newInstance(newGenotype);
    }
}
