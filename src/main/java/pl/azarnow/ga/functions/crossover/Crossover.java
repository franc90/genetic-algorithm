package pl.azarnow.ga.functions.crossover;

import pl.azarnow.ga.chromosomes.Chromosome;

public interface Crossover {

    Chromosome crossover(Chromosome c1, Chromosome c2);

}
