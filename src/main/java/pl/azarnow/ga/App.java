package pl.azarnow.ga;

import pl.azarnow.ga.chromosomes.ChromosomeType;
import pl.azarnow.ga.chromosomes.factories.BusinessmanFactory;
import pl.azarnow.ga.chromosomes.factories.ClientFactory;
import pl.azarnow.ga.chromosomes.factories.SimpleChromosomeFactory;
import pl.azarnow.ga.chromosomes.factories.strategies.ChromosomeFactoryStrategy;
import pl.azarnow.ga.experiments.CoevolutionarySharedNichingExperiment;
import pl.azarnow.ga.experiments.DeterministicCrowdingExperiment;
import pl.azarnow.ga.experiments.ProbabilisticCrowdingExperiment;
import pl.azarnow.ga.functions.fitness.FitnessFunctionBuilder;
import pl.azarnow.ga.utils.CommonSettings;

import java.io.FileNotFoundException;

public class App {

    public static void main(String[] args) throws FileNotFoundException {
        initSettings();

        new DeterministicCrowdingExperiment()
                .withRepetitions(1)
                .withTotalPopulations(10, 50, 100, 500)
                .withMutationProbabilities(.1, .5, .7, .99)
                .withIterations(10, 50, 100, 200)
                .withIslandIterations(2, 5)
                .withOutputFile("deterministicCrowding.txt")
                .experiment();

        new ProbabilisticCrowdingExperiment()
                .withRepetitions(1)
                .withTotalPopulations(10, 50, 100, 500)
                .withMutationProbabilities(.1, .5, .7, .99)
                .withIterations(10, 50, 100, 200)
                .withIslandIterations(2, 5)
                .withOutputFile("probabilisticCrowding.txt")
                .experiment();

        new CoevolutionarySharedNichingExperiment()
                .withRepetitions(1)
                .withBusinessmenPopulations(5, 10)
                .withClientPopulations(20, 50, 200)
                .withMutationProbabilities(.1, .5, .7, .99)
                .withIterations(10, 50, 100, 200)
                .withIslandIterations(2, 5)
                .withOutputFile("coev.txt")
                .experiment();

    }

    public static void initSettings() {
        CommonSettings.setChromosomeFactoryStrategy(new ChromosomeFactoryStrategy.ChromosomeFactoryStrategyBuilder()
                        .withFactoryFor(new SimpleChromosomeFactory(), ChromosomeType.Simple, ChromosomeType.Default)
                        .withFactoryFor(new BusinessmanFactory(), ChromosomeType.Businessman)
                        .withFactoryFor(new ClientFactory(), ChromosomeType.Client)
                        .build()
        );
        CommonSettings.setFitnessFunction(
                new FitnessFunctionBuilder()
                        .function(FitnessFunctionBuilder.FunctionType.Rastrigin)
                        .setParametersTranslation(2., 2.)
                        .build()
        );
        CommonSettings.setRastriginConstant(10);

        CommonSettings.setGeneMutationProbability(.8);
        CommonSettings.setSelectedOnceInIteration(true);

        CommonSettings.setLowerBound(-10.12);
        CommonSettings.setUpperBound(5.12);
        CommonSettings.setGenotypeSize(2);
        CommonSettings.setTournamentSize(4);
        CommonSettings.setIslandsNumber(5);
    }
}
