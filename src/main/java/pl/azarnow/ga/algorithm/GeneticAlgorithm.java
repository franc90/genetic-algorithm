package pl.azarnow.ga.algorithm;

import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.population.Population;
import pl.azarnow.ga.utils.CommonSettings;

public class GeneticAlgorithm extends AbstractGeneticAlgorithm {
    @Override
    protected Chromosome evolvePopulation(int iterationNr) {
        Population newPopulation = new Population();

        for (int i = 0; i < CommonSettings.getPopulationSize(); ++i) {
            Chromosome c1 = select(currentPopulation);
            Chromosome c2 = select(currentPopulation);

            Chromosome newChromosome = crossover(c1, c2);
            newPopulation.saveChromosome(i, newChromosome);
        }

        for (int i = 0; i < newPopulation.size(); ++i) {
            if (random.nextDouble() < CommonSettings.getMutationProbability()) {
                Chromosome mutated = mutate(newPopulation.getChromosome(i), iterationNr);
                newPopulation.saveChromosome(i, mutated);
            }
        }
        this.currentPopulation = newPopulation;
        return currentPopulation.getFittest();
    }
}
