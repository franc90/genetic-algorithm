package pl.azarnow.ga.algorithm;

import pl.azarnow.ga.chromosomes.Chromosome;

import java.util.*;

public class ProbabilisticCrowdingIslandModel extends DeterministicCrowdingIslandModel {

    private Random r = new Random();

    @Override
    protected Collection<? extends Chromosome> selectBestTwo(List<Chromosome> chromosomes) {
        Chromosome c0 = chromosomes.get(0);
        Chromosome c1 = chromosomes.get(1);
        Chromosome c2 = chromosomes.get(2);
        Chromosome c3 = chromosomes.get(3);

        Chromosome semiFinalWinner1 = compare(c0, c1);
        Chromosome semiFinalWinner2 = compare(c2, c3);

        Chromosome winner = compare(semiFinalWinner1, semiFinalWinner2);

        Chromosome looser;
        Chromosome second;
        if (winner == semiFinalWinner1) {
            if (semiFinalWinner1 == c0) {
                looser = c1;
            } else {
                looser = c0;
            }
            second = compare(semiFinalWinner2, looser);
        } else {
            if (semiFinalWinner2 == c2) {
                looser = c3;
            } else {
                looser = c2;
            }
            second = compare(semiFinalWinner1, looser);
        }

        return Arrays.asList(winner, second);
    }

    private Chromosome compare(Chromosome c1, Chromosome c2) {
        double f1 = 1 / c1.getFitness();
        double f2 = 1 / c2.getFitness();
        double sum = f1 + f2;

        double p1 = f1 / sum;

        double random = r.nextDouble();
        if (random < p1) {
            return c1;
        }
        return c2;
    }
}
