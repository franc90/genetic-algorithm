package pl.azarnow.ga.algorithm;

import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.population.Population;
import pl.azarnow.ga.utils.CommonSettings;

import java.util.*;

public class PreselectionIslandModel extends IslandModelGeneticAlgorithm {

    @Override
    protected Chromosome evolvePopulation(int iterationNr) {
        if (iterationNr == 1) {
            prepareIslands();
        }

        List<Population> newPopulations = new ArrayList<>(islands.size());
        islands.parallelStream()
                .map(p -> {
                    Population newPopulation = new Population();
                    Population currentPopulation = p;

                    for (int i = 0; i < CommonSettings.getPopulationSize(); i += 2) {
                        if(currentPopulation.getChromosomes().size() < 2) {
                            continue;
                        }

                        Chromosome c1 = select(currentPopulation);
                        Chromosome c2 = select(currentPopulation);

                        Chromosome newChromosome = crossover(c1, c2);

                        Set<Chromosome> chromosomes = preselection(c1, c2, newChromosome);
                        currentPopulation.getChromosomes().removeAll(chromosomes);
                        newPopulation.getChromosomes().addAll(chromosomes);
                    }

                    for (int i = 0; i < newPopulation.size(); ++i) {
                        if (random.nextDouble() < CommonSettings.getMutationProbability()) {
                            Chromosome mutated = mutate(newPopulation.getChromosome(i), iterationNr);
                            newPopulation.saveChromosome(i, mutated);
                        }
                    }
                    currentPopulation = newPopulation;
                    newPopulation = new Population();

                    return currentPopulation;
                })
                .forEach(newPopulations::add);

        islands = migration.migrate(newPopulations);

        if (iterationNr == CommonSettings.getIslandsNumber()) {
            currentPopulation = mergeIslands();
        }

        return bestChromosome();
    }

    private Set<Chromosome> preselection(Chromosome... chomosomesArr) {
        Set<Chromosome> chromosomes = new HashSet<>(Arrays.asList(chomosomesArr));

        Chromosome min = Collections.min(chromosomes);
        chromosomes.remove(min);

        return chromosomes;
    }
}
