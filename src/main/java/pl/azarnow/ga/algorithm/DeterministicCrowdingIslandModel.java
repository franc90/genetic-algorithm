package pl.azarnow.ga.algorithm;

import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.population.Population;
import pl.azarnow.ga.utils.CommonSettings;

import java.util.*;

public class DeterministicCrowdingIslandModel extends IslandModelGeneticAlgorithm {

    @Override
    protected Chromosome evolvePopulation(int iterationNr) {
        if (iterationNr == 1) {
            prepareIslands();
            for (Population island : islands) {
                CommonSettings.getAllResults().addResult(0, island);
            }
        }

        List<Population> newPopulations = islands;

        for (int q = 0; q < CommonSettings.getIslandsIterations(); ++q) {
            List<Population> tmpPop = new ArrayList<>();
            for (Population population : newPopulations) {
                int populationSize = population.size();
                Population newPopulation = new Population();
                newPopulation.setPopulationNr(population.getPopulationNr());

                for (int i = 0; i < populationSize; i += 2) {
                    if (population.getChromosomes().size() < 2) {
                        continue;
                    }

                    Chromosome p1 = select(population);
                    Chromosome p2 = select(population);
                    Chromosome c1 = crossover(p1, p2);
                    Chromosome c2 = crossover(p1, p2);

                    newPopulation.getChromosomes().addAll(selectBestTwo(Arrays.asList(p1, p2, c1, c2)));
                }

                for (int i = 0; i < newPopulation.size(); ++i) {
                    if (random.nextDouble() < CommonSettings.getMutationProbability()) {
                        Chromosome mutated = mutate(newPopulation.getChromosome(i), iterationNr);
                        newPopulation.saveChromosome(i, mutated);
                    }
                }

                tmpPop.add(newPopulation);
            }
            newPopulations = tmpPop;
        }

//        islands = migration.migrate(newPopulations);
        islands = newPopulations;
        newPopulations.forEach(e -> CommonSettings.getAllResults().addResult(iterationNr, e));

        if (iterationNr == CommonSettings.getIslandsNumber()) {
            currentPopulation = mergeIslands();
        }

        return bestChromosome();
    }

    protected Collection<? extends Chromosome> selectBestTwo(List<Chromosome> chromosomes) {
        List<Chromosome> returnList = new ArrayList<>();
        Collections.sort(chromosomes);

        returnList.add(chromosomes.get(0));
        returnList.add(chromosomes.get(1));

        return returnList;
    }


}
