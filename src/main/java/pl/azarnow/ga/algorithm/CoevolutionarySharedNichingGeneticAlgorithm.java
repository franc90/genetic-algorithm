package pl.azarnow.ga.algorithm;

import pl.azarnow.ga.chromosomes.*;
import pl.azarnow.ga.population.CoevolutionarySharedNichingPopulation;
import pl.azarnow.ga.population.Population;
import pl.azarnow.ga.utils.CommonSettings;
import pl.azarnow.ga.utils.WorstChromosomeChooser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class CoevolutionarySharedNichingGeneticAlgorithm extends AbstractGeneticAlgorithm {

    private WorstChromosomeChooser chooser = new WorstChromosomeChooser();

    @Override
    protected Chromosome evolvePopulation(int iterationNr) {
        if (iterationNr == 1) {
            CommonSettings.getAllResults().addResult(0, currentPopulation);
        }

        CoevolutionarySharedNichingPopulation currPopulation = (CoevolutionarySharedNichingPopulation) currentPopulation;

        Population<Client> clients = getPopulation(currPopulation, ChromosomeType.Client, Client.class);
        Population<Businessman> businessmen = getPopulation(currPopulation, ChromosomeType.Businessman, Businessman.class);
        clients.setPopulationNr(1);
        businessmen.setPopulationNr(2);

        findBusinessmen(clients, businessmen);
        computeFitness(clients, businessmen);

        clients = crossover(clients);
        mutation(clients, iterationNr);
        businessmen = crossover(businessmen);
        mutation(businessmen, iterationNr);

        findBusinessmen(clients, businessmen);
        computeFitness(clients, businessmen);

        clients = chooseBest(clients);
        businessmen = chooseBest(businessmen);

        removeParents(clients, businessmen);

        saveChromosomesForPlotting(iterationNr, businessmen);
        saveChromosomesForPlotting(iterationNr, clients);

        addToCurrentPopulation(clients, businessmen);

        return new CoevolutionarySharedNichingPopulation(clients.getChromosomes()).getFittest();
    }

    private static <T extends Chromosome> Population<T> getPopulation(CoevolutionarySharedNichingPopulation currPopulation, ChromosomeType chromosomeType, Class<T> type) {
        List<T> chromosomes = new ArrayList<>();

        for (Chromosome chromosome : (List<Chromosome>) currPopulation.getChromosomes()) {
            if (chromosome.getChromosomeType().equals(chromosomeType)) {
                chromosomes.add(type.cast(chromosome));
            }
        }

        return new Population<T>(chromosomes);
    }

    private void findBusinessmen(Population<Client> clients, Population<Businessman> businessmen) {
        for (Businessman businessman : businessmen.getChromosomes()) {
            businessman.getClients().clear();
        }

        for (Client client : clients.getChromosomes()) {
            client.findClosestBuisnessman(businessmen);
        }
    }

    private void computeFitness(Population... chromosomes) {
        for (Population<CoevolutionarySharedNichingChromosome> population : chromosomes) {
            for (CoevolutionarySharedNichingChromosome chromosome : population.getChromosomes()) {
                chromosome.computeFitness();
            }
        }
    }

    private Population crossover(Population population) {
        Population newPopulation = new Population(false);
        newPopulation.setPopulationNr(population.getPopulationNr());

        while (population.size() > 1) {
            Chromosome p1 = select(population);
            Chromosome p2 = select(population);
            Chromosome c1 = crossover(p1, p2);
            Chromosome c2 = crossover(p1, p2);

            c1.getParents().add(p1);
            c1.getParents().add(p2);
            c2.getParents().add(p1);
            c2.getParents().add(p2);

            newPopulation.getChromosomes().addAll(Arrays.asList(p1, p2, c1, c2));
        }

        return newPopulation;
    }

    private void mutation(Population population, int iterationNr) {
        for (int i = 0; i < population.size(); ++i) {
            if (random.nextDouble() < CommonSettings.getMutationProbability()) {
                Chromosome oldChromosome = population.getChromosome(i);
                Chromosome mutated = mutate(oldChromosome, iterationNr);
                mutated.getParents().addAll(oldChromosome.getParents());
                population.saveChromosome(i, mutated);
            }
        }
    }

    private Population chooseBest(Population population) {
        Population<Chromosome> parents = getPopulation(population, true);
        Population<Chromosome> children = getPopulation(population, false);

        Iterator<Chromosome> kids = children.getChromosomes().iterator();
        while (kids.hasNext()) {
            Chromosome child = kids.next();

            if (isWorseThanParents(child, parents)) {
                kids.remove();
            }
        }

        Population<Chromosome> returnPopulation = new Population<>(parents.getChromosomes());
        returnPopulation.setPopulationNr(population.getPopulationNr());
        returnPopulation.getChromosomes().addAll(children.getChromosomes());

        return returnPopulation;
    }

    private Population getPopulation(Population<Chromosome> population, boolean noParents) {
        List<Chromosome> chromosomes = new ArrayList<>();

        for (Chromosome chromosome : population.getChromosomes()) {
            if (chromosome.getParents().isEmpty() == noParents) {
                chromosomes.add(chromosome);
            }
        }

        return new Population<>(chromosomes);
    }

    private boolean isWorseThanParents(Chromosome child, Population<Chromosome> parents) {
        chooser.clear();
        chooser.add(child);
        chooser.addAll(child.getParents());
        Chromosome worstChromosome = chooser.findWorstChromosome();

        if (worstChromosome != child) {
            if (parents.getChromosomes().contains(worstChromosome)) {
                parents.getChromosomes().remove(worstChromosome);
                return false;
            }
            child.getParents().remove(worstChromosome);
            return isWorseThanParents(child, parents);
        }

        return true;
    }

    private void removeParents(Population... populations) {
        for (Population<Chromosome> population : populations) {
            for (Chromosome chromosome : population.getChromosomes()) {
                chromosome.getParents().clear();
            }
        }
    }

    private void saveChromosomesForPlotting(int iterationNr, Population originalPopulation) {
        List<Chromosome> chromosomes = new ArrayList<>(originalPopulation.getChromosomes().size());
        for (CoevolutionarySharedNichingChromosome chromosome : (List<CoevolutionarySharedNichingChromosome>)originalPopulation.getChromosomes()) {
            chromosomes.add(new Chromosome(chromosome.getGenotype()));
        }

        Population<Chromosome> population = new Population<>(chromosomes);
        population.setPopulationNr(originalPopulation.getPopulationNr());
        CommonSettings.getAllResults().addResult(iterationNr, population);
    }

    private void addToCurrentPopulation(Population<Client> clients, Population<Businessman> businessmen) {
        currentPopulation.getChromosomes().clear();
        currentPopulation.getChromosomes().addAll(businessmen.getChromosomes());
        currentPopulation.getChromosomes().addAll(clients.getChromosomes());
    }
}
