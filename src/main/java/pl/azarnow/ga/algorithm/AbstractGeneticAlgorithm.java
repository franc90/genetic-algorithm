package pl.azarnow.ga.algorithm;

import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.functions.crossover.Crossover;
import pl.azarnow.ga.functions.migration.Migration;
import pl.azarnow.ga.functions.mutation.Mutation;
import pl.azarnow.ga.functions.selection.Selection;
import pl.azarnow.ga.population.Population;
import pl.azarnow.ga.utils.CommonSettings;

import java.util.Random;

public abstract class AbstractGeneticAlgorithm implements Algorithm {

    protected Random random = new Random();

    protected Crossover crossover;

    protected Mutation mutation;

    protected Selection selection;

    protected Migration migration;

    protected Population<Chromosome> currentPopulation;

    @Override
    public Population evolve(Population population) {
        currentPopulation = population;

        Chromosome chromosome = null;
        for (int i = 0; i < CommonSettings.getTotalIterations(); i++) {
            chromosome = evolvePopulation(i + 1);
//            System.out.println((i+1) + ". " + chromosome);
        }

//        System.out.println(chromosome);

        return currentPopulation;
    }

    protected abstract Chromosome evolvePopulation(int iterationNr);

    protected Chromosome crossover(Chromosome c1, Chromosome c2) {
        return crossover.crossover(c1, c2);
    }

    protected Chromosome mutate(Chromosome chromosome, int iterationNr) {
        return mutation.mutate(chromosome, iterationNr);
    }

    protected Chromosome select(Population population) {
        return selection.select(population);
    }

    @Override
    public void setCrossover(Crossover crossover) {
        this.crossover = crossover;
    }

    @Override
    public void setMutation(Mutation mutation) {
        this.mutation = mutation;
    }

    @Override
    public void setSelection(Selection selection) {
        this.selection = selection;
    }

    @Override
    public void setMigration(Migration migration) {
        this.migration = migration;
    }
}
