package pl.azarnow.ga.algorithm;

import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.population.Population;
import pl.azarnow.ga.utils.CommonSettings;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class IslandModelGeneticAlgorithm extends AbstractGeneticAlgorithm {

    List<Population> islands = new ArrayList<>(CommonSettings.getIslandsNumber());

    @Override
    protected Chromosome evolvePopulation(int iterationNr) {
        if (iterationNr == 1) {
            prepareIslands();
        }

        List<Population> newPopulations = new ArrayList<>(islands.size());
        islands.parallelStream()
                .map(p -> {
                    Population newPopulation = new Population();
                    Population currentPopulation = p;

                    for (int i = 0; i < CommonSettings.getPopulationSize(); ++i) {
                        Chromosome c1 = select(currentPopulation);
                        Chromosome c2 = select(currentPopulation);

                        Chromosome newChromosome = crossover(c1, c2);
                        newPopulation.saveChromosome(i, newChromosome);
                    }

                    for (int i = 0; i < newPopulation.size(); ++i) {
                        if (random.nextDouble() < CommonSettings.getMutationProbability()) {
                            Chromosome mutated = mutate(newPopulation.getChromosome(i), iterationNr);
                            newPopulation.saveChromosome(i, mutated);
                        }
                    }
                    currentPopulation = newPopulation;
                    newPopulation = new Population();

                    return currentPopulation;
                })
                .forEach(newPopulations::add);

        islands = migration.migrate(newPopulations);

        if (iterationNr == CommonSettings.getIslandsNumber()) {
            currentPopulation = mergeIslands();
        }

        return bestChromosome();
    }

    protected void prepareIslands() {
        List<List<Chromosome>> tmpPopulations = new ArrayList<>(CommonSettings.getIslandsNumber());
        for (int i = 0; i < CommonSettings.getIslandsNumber(); i++) {
            tmpPopulations.add(new LinkedList<>());
        }

        int cnt = 0;
        for (Chromosome chromosome : currentPopulation.getChromosomes()) {
            tmpPopulations.get(cnt++).add(chromosome);
            cnt = cnt % CommonSettings.getIslandsNumber();
        }

        for (int i = 0; i < CommonSettings.getIslandsNumber(); i++) {
            Population p = new Population(tmpPopulations.get(i));
            p.setPopulationNr(i);
            islands.add(p);
        }
    }

    protected Population mergeIslands() {
        List<Chromosome> allChromosomes = new LinkedList<>();

        for (Population population : islands) {
            allChromosomes.addAll(population.getChromosomes());
        }

        return new Population(allChromosomes);
    }

    protected Chromosome bestChromosome() {
        Chromosome best = islands.get(0).getFittest();

        for (int i = 1; i < islands.size(); i++) {
            Population population = islands.get(i);
            Chromosome candidate = population.getFittest();
            if (candidate.getFitness() <= best.getFitness()) {
                best = candidate;
            }
        }

        return best;
    }
}
