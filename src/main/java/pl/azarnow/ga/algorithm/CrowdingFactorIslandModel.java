package pl.azarnow.ga.algorithm;

import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.population.Population;
import pl.azarnow.ga.utils.CommonSettings;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CrowdingFactorIslandModel extends IslandModelGeneticAlgorithm {

    @Override
    protected Chromosome evolvePopulation(int iterationNr) {
        if (iterationNr == 1) {
            prepareIslands();
        }

        List<Population> newPopulations = new ArrayList<>(islands.size());
        islands.parallelStream()
                .map(p -> {
                    Population newPopulation = new Population();
                    Population currentPopulation = p;
                    newPopulation.getChromosomes().addAll(currentPopulation.getChromosomes());

                    for (int i = 0; i < CommonSettings.getPopulationSize(); i += 2) {
                        if(currentPopulation.getChromosomes().size() < 2) {
                            continue;
                        }

                        Chromosome c1 = select(currentPopulation);
                        Chromosome c2 = select(currentPopulation);

                        Chromosome newChromosome = crossover(c1, c2);

                        Chromosome chosenChromosome = crowdingFactor(newChromosome, currentPopulation);
                        newPopulation.getChromosomes().remove(chosenChromosome);
                        newPopulation.getChromosomes().add(newChromosome);
                    }

                    for (int i = 0; i < newPopulation.size(); ++i) {
                        if (random.nextDouble() < CommonSettings.getMutationProbability()) {
                            Chromosome mutated = mutate(newPopulation.getChromosome(i), iterationNr);
                            newPopulation.saveChromosome(i, mutated);
                        }
                    }

                    return newPopulation;
                })
                .forEach(newPopulations::add);

        islands = migration.migrate(newPopulations);

        if (iterationNr == CommonSettings.getIslandsNumber()) {
            currentPopulation = mergeIslands();
        }

        return bestChromosome();
    }

    // It's hard to use Hamming's Metric - used to check how many bits need to be changed to get the same sequence
    // Therefore standard euclidean metric is used.
    private Chromosome crowdingFactor(Chromosome newChromosome, Population currentPopulation) {
        List<Chromosome> chromosomes = getRandomNr(currentPopulation);

        Chromosome next = null;
        double dist = Double.MAX_VALUE;

        for (Chromosome chromosome : chromosomes) {
            double newDist = newChromosome.distance(chromosome);
            if (newDist < dist) {
                dist = newDist;
                next = chromosome;
            }
        }

        return next;
    }

    private List<Chromosome> getRandomNr(Population population) {
        Random r = new Random();
        List<Chromosome> tmpList = new ArrayList<>(population.getChromosomes());

        int size = r.nextInt(population.size());
        List<Chromosome> returnList = new ArrayList<>(size);

        for (int i = 0 ; i < size && !tmpList.isEmpty() ; ++i) {
            returnList.add(tmpList.remove(r.nextInt(tmpList.size())));
        }

        return returnList;
    }


}
