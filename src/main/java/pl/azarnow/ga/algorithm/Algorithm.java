package pl.azarnow.ga.algorithm;

import pl.azarnow.ga.functions.crossover.Crossover;
import pl.azarnow.ga.functions.migration.Migration;
import pl.azarnow.ga.functions.mutation.Mutation;
import pl.azarnow.ga.functions.selection.Selection;
import pl.azarnow.ga.population.Population;

public interface Algorithm {

    Population evolve(Population population);

    void setCrossover(Crossover crossover);

    void setMutation(Mutation mutation);

    void setSelection(Selection selection);

    void setMigration(Migration migration);

}
