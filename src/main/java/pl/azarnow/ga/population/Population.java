package pl.azarnow.ga.population;

import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.utils.CommonSettings;

import java.util.ArrayList;
import java.util.List;

public class Population<T extends Chromosome> {

    protected int populationNr;

    protected List<T> chromosomes;

    public Population() {
        this(false);
    }

    public Population(boolean initialize) {
        chromosomes = new ArrayList<>(CommonSettings.getPopulationSize());
        if (initialize) {
            for (int i = 0; i < CommonSettings.getPopulationSize(); i++) {
                T chromosome = (T) CommonSettings.getChromosomeFactoryStrategy().getDefaultFactory().newInstance();
                chromosomes.add(chromosome);
            }
        }
    }

    public Population(List<T> chromosomes) {
        this.chromosomes = chromosomes;
    }

    public Chromosome getChromosome(int index) {
        return chromosomes.get(index);
    }

    public Chromosome getFittest() {
        Chromosome fittest = chromosomes.get(0);
        for (int i = 1; i < size(); i++) {
            if (fittest.getFitness() >= getChromosome(i).getFitness()) {
                fittest = getChromosome(i);
            }
        }
        return fittest;
    }

    public int getPopulationNr() {
        return populationNr;
    }

    public void setPopulationNr(int populationNr) {
        this.populationNr = populationNr;
    }

    public int size() {
        return chromosomes.size();
    }

    public void saveChromosome(int index, T chromosome) {
        if (index == chromosomes.size()) {
            chromosomes.add(chromosome);
        } else {
            chromosomes.set(index, chromosome);
        }
    }

    public List<T> getChromosomes() {
        return chromosomes;
    }
}
