package pl.azarnow.ga.population;

import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.chromosomes.ChromosomeType;
import pl.azarnow.ga.chromosomes.CoevolutionarySharedNichingChromosome;
import pl.azarnow.ga.chromosomes.factories.AbstractChromosomeFactory;
import pl.azarnow.ga.utils.CommonSettings;

import java.util.ArrayList;
import java.util.List;

public class CoevolutionarySharedNichingPopulation extends Population {

    public CoevolutionarySharedNichingPopulation(boolean initialize) {
        chromosomes = new ArrayList<>(CommonSettings.getPopulationSize());
        if (initialize) {
            for (int chromosomeNr = 0; chromosomeNr < CommonSettings.getPopulationSize(); ++chromosomeNr) {
                Chromosome chromosome = getFactory(chromosomeNr).newInstance();
                chromosomes.add(chromosome);
            }
        }
    }

    public CoevolutionarySharedNichingPopulation(List chromosomes) {
        this.chromosomes = chromosomes;
    }

    @Override
    public Chromosome getFittest() {
        CoevolutionarySharedNichingChromosome fittest = (CoevolutionarySharedNichingChromosome) chromosomes.get(0);
        for (int i = 1; i < size(); i++) {
            if (fittest.getOriginalFitness() >= ((CoevolutionarySharedNichingChromosome) getChromosome(i)).getOriginalFitness()) {
                fittest = (CoevolutionarySharedNichingChromosome) getChromosome(i);
            }
        }
        return fittest;
    }

    private AbstractChromosomeFactory getFactory(int chromosomeNr) {
        if (chromosomeNr < CommonSettings.getBusinessmenPopulationSize()) {
            return CommonSettings.getChromosomeFactoryStrategy().getFactory(ChromosomeType.Businessman);
        }
        return CommonSettings.getChromosomeFactoryStrategy().getFactory(ChromosomeType.Client);
    }
}
