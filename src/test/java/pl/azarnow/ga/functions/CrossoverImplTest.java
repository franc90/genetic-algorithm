package pl.azarnow.ga.functions;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.chromosomes.ChromosomeType;
import pl.azarnow.ga.chromosomes.factories.BusinessmanFactory;
import pl.azarnow.ga.chromosomes.factories.ClientFactory;
import pl.azarnow.ga.chromosomes.factories.SimpleChromosomeFactory;
import pl.azarnow.ga.chromosomes.factories.strategies.ChromosomeFactoryStrategy;
import pl.azarnow.ga.functions.crossover.CrossoverImpl;
import pl.azarnow.ga.utils.CommonSettings;

public class CrossoverImplTest {

    @BeforeClass
    public static void initClass() {
        CommonSettings.setChromosomeFactoryStrategy(new ChromosomeFactoryStrategy.ChromosomeFactoryStrategyBuilder()
                        .withFactoryFor(new SimpleChromosomeFactory(), ChromosomeType.Simple, ChromosomeType.Default)
                        .withFactoryFor(new BusinessmanFactory(), ChromosomeType.Businessman)
                        .withFactoryFor(new ClientFactory(), ChromosomeType.Client)
                        .build()
        );
        CommonSettings.setGenotypeSize(2);
        CommonSettings.setLowerBound(4.3);
        CommonSettings.setUpperBound(21.2);
        CommonSettings.setTotalIterations(10);
        CommonSettings.setGeneMutationProbability(.65);
        CommonSettings.setMutationProbability(.35);
    }

    @Test
    public void crossoverTest() {
        Chromosome c1 = CommonSettings.getChromosomeFactoryStrategy().getDefaultFactory().newInstance();
        Chromosome c2 = CommonSettings.getChromosomeFactoryStrategy().getDefaultFactory().newInstance();

        Chromosome c3 = new CrossoverImpl().crossover(c1, c2);

        for (int i = 0; i < CommonSettings.getGenotypeSize(); i++) {
            Double c1Val = c1.getGenotype().get(i);
            Double c2Val = c2.getGenotype().get(i);
            Double c3Val = c3.getGenotype().get(i);

            Assert.assertTrue((c1Val <= c3Val && c3Val <= c2Val) || (c2Val <= c3Val && c3Val <= c1Val));
        }
    }
}
