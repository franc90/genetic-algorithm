package pl.azarnow.ga.functions.selection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.functions.selection.roulette.RouletteWheel;
import pl.azarnow.ga.population.Population;

import java.util.Random;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RouletteWheelSelectionTest {

    RouletteWheelSelection selection = new RouletteWheelSelection();

    RouletteWheel wheel = new RouletteWheel();

    @Mock
    private Chromosome c1;
    @Mock
    private Chromosome c2;

    @Mock
    private Random r;

    private Population p;

    @Before
    public void init() {
        when(c1.getFitness()).thenReturn(3.);
        when(c2.getFitness()).thenReturn(1.);
        p = new Population();

        when(r.nextDouble()).thenReturn(1.);
        wheel.setRandom(r);

        p.getChromosomes().add(c1);
        selection.setRouletteWheel(wheel);
    }

    @Test
    public void testSelectingSingleItem() {
        Chromosome chromosome = selection.select(p);

        assertNotNull(chromosome);
        assertEquals(chromosome, c1);
    }

    @Test
    public void testSelectingSecondItem() {
        p.getChromosomes().add(c2);

        Chromosome chromosome = selection.select(p);

        assertNotNull(chromosome);
        assertEquals(chromosome, c2);
    }
}