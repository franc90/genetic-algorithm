package pl.azarnow.ga.functions;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.chromosomes.ChromosomeType;
import pl.azarnow.ga.chromosomes.factories.BusinessmanFactory;
import pl.azarnow.ga.chromosomes.factories.ClientFactory;
import pl.azarnow.ga.chromosomes.factories.SimpleChromosomeFactory;
import pl.azarnow.ga.chromosomes.factories.strategies.ChromosomeFactoryStrategy;
import pl.azarnow.ga.functions.mutation.DynamicMutation;
import pl.azarnow.ga.utils.CommonSettings;

public class DynamicMutationTest {

    @BeforeClass
    public static void initClass() {
        CommonSettings.setChromosomeFactoryStrategy(new ChromosomeFactoryStrategy.ChromosomeFactoryStrategyBuilder()
                        .withFactoryFor(new SimpleChromosomeFactory(), ChromosomeType.Simple, ChromosomeType.Default)
                        .withFactoryFor(new BusinessmanFactory(), ChromosomeType.Businessman)
                        .withFactoryFor(new ClientFactory(), ChromosomeType.Client)
                        .build()
        );
        CommonSettings.setGenotypeSize(2);
        CommonSettings.setLowerBound(4.3);
        CommonSettings.setUpperBound(21.2);
        CommonSettings.setTotalIterations(10);
        CommonSettings.setGeneMutationProbability(.65);
        CommonSettings.setMutationProbability(.35);
    }

    @Test
    public void mutationTest() {
        Chromosome chromosome = CommonSettings.getChromosomeFactoryStrategy().getDefaultFactory().newInstance();
        double oldFitness = chromosome.getFitness();
        chromosome = new DynamicMutation().mutate(chromosome, 1);
        double newFitness = chromosome.getFitness();
        Assert.assertNotEquals(oldFitness, newFitness);
    }
}
