package pl.azarnow.ga.results;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.functions.fitness.RastriginFitnessFunction;
import pl.azarnow.ga.population.Population;
import pl.azarnow.ga.utils.CommonSettings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AllResultsTest {

    private AllResults r;
    private Population p1;

    @BeforeClass
    public static void init() {
        CommonSettings.setFitnessFunction(new RastriginFitnessFunction());
    }

    @Before
    public void before() {
        r = new AllResults();

        List<Double> genes = new ArrayList<>();
        genes.add(1.);
        genes.add(2.);

        p1 = new Population();
        Chromosome c1 = new Chromosome(genes);
        Chromosome c2 = new Chromosome(genes);
        Chromosome c3 = new Chromosome(genes);
        Chromosome c4 = new Chromosome(genes);
        p1.getChromosomes().addAll(Arrays.asList(c1, c2, c3, c4));
    }

    @Test
    public void testAddResult() throws Exception {
        r.addResult(13, p1);

        ResultSet ret = r.getResultByIteration(12);
        Assert.assertNull(ret);

        ret = r.getResultByIteration(13);
        Assert.assertNotNull(ret);
    }
}