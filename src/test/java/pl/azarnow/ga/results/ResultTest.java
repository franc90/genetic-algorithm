package pl.azarnow.ga.results;

import org.junit.*;
import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.functions.fitness.RastriginFitnessFunction;
import pl.azarnow.ga.population.Population;
import pl.azarnow.ga.utils.CommonSettings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ResultTest {

    private Result r;

    private Population p1;

    private Chromosome c1;

    @BeforeClass
    public static void init() {
        CommonSettings.setFitnessFunction(new RastriginFitnessFunction());
    }

    @Before
    public void before() {
        r = new Result(1);

        List<Double> genes = new ArrayList<>();
        genes.add(1.);
        genes.add(2.);

        p1 = new Population();
        c1 = new Chromosome(genes);
        Chromosome c2 = new Chromosome(genes);
        Chromosome c3 = new Chromosome(genes);
        Chromosome c4 = new Chromosome(genes);
        p1.getChromosomes().addAll(Arrays.asList(c1, c2, c3, c4));
    }

    @Test
    public void testAddPoint() throws Exception {
        r.addPoint(c1);

        List<Point> points = r.getPoints();
        Assert.assertNotNull(points);
        Assert.assertEquals(points.size(), 1);
    }



    @Test
    public void testAddPoints() throws Exception {
        r.addPoints(p1);

        List<Point> points = r.getPoints();
        int populationNr = r.getPopulationNr();

        Assert.assertNotNull(points);
        Assert.assertEquals(populationNr, 1);
        Assert.assertEquals(points.size(), 4);
    }

}