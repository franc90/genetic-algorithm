package pl.azarnow.ga.results;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import pl.azarnow.ga.chromosomes.Chromosome;
import pl.azarnow.ga.functions.fitness.RastriginFitnessFunction;
import pl.azarnow.ga.population.Population;
import pl.azarnow.ga.utils.CommonSettings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ResultSetTest {

    private ResultSet rs;

    private Population p1;

    @BeforeClass
    public static void init() {
        CommonSettings.setFitnessFunction(new RastriginFitnessFunction());
    }

    @Before
    public void before() {
        rs = new ResultSet(1);

        List<Double> genes = new ArrayList<>();
        genes.add(1.);
        genes.add(2.);

        p1 = new Population();
        Chromosome c1 = new Chromosome(genes);
        Chromosome c2 = new Chromosome(genes);
        Chromosome c3 = new Chromosome(genes);
        Chromosome c4 = new Chromosome(genes);
        p1.getChromosomes().addAll(Arrays.asList(c1, c2, c3, c4));
    }


    @Test
    public void testAddOnceResult() throws Exception {
        rs.addResult(p1);

        List<Result> results = rs.getResults();

        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1);
    }

    @Test
    public void testAddTwiceResult() throws Exception {
        rs.addResult(p1);

        List<Result> results = rs.getResults();

        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1);

        rs.addResult(p1);

        List<Result> results2 = rs.getResults();

        Assert.assertEquals(results, results2);
    }

    @Test
    public void testSize() throws Exception {
        rs.addResult(p1);

        int size = rs.size();

        Assert.assertEquals(size, 4);
    }
}